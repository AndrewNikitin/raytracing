SOURCE_FILES= source/cpu_renderer.cuh  \
			  source/gpu_renderer.cuh  \
			  source/light.cuh  \
			  source/main.cu  \
			  source/material.cuh  \
			  source/object.cuh  \
			  source/ray.cuh  \
			  source/scene.cuh  \
			  source/image.cuh  \
			  source/vector.cuh \
			  source/camera.cuh \
			  source/ssaa.cuh \
			  source/texture.cuh

NVCC_FLAGS=
CPP_FLAGS=-std=c++11
BIN=raytracer

all: ${SOURCE_FILES}
	nvcc ${NVCC_FLAGS} ${CPP_FLAGS} -o ${BIN} source/main.cu
