#ifndef PGP_CW_V3_SCENE_H
#define PGP_CW_V3_SCENE_H

#include <vector>

#include "vector.cuh"
#include "object.cuh"
#include "light.cuh"
#include "texture.cuh"

struct Scene
{
    int width;
    int height;


    int objects_num;
    Vec3f center[3];
    Vec3f color[3];
    float radius[3];
    float refract[3];
    float transparency[3];
    int lights_per_edge[3];
    
    Vec3f floor_coords[4];

    int n_lights;
    Vec3f light_pos[4];
    Vec3f light_col[4];

    int materials_num;
    Object **objects;
    Light **lights;
    Material **materials;

    Texture **textures;
};


#endif //PGP_CW_V3_SCENE_H
