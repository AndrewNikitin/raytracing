#ifndef PGP_CW_V3_OBJECT_H
#define PGP_CW_V3_OBJECT_H

#include "vector.cuh"
#include "material.cuh"
#include "ray.cuh"
#include "texture.cuh"

const float T_MIN = 1e-3;
const float T_MAX = 1e+6;

struct HitRecord {
    __host__ __device__ HitRecord() : t(0), m(nullptr), u(0), v(0), emitted(false) {}
    float t;
    Vec3f p;
    Vec3f n;
    float u, v;
    bool emitted;
    Material *m;
};

__host__ __device__
inline float Dist(const Vec3f &p, const Vec3f &a, const Vec3f &b)
{
    return cross(b - a, p - a).length() / (b - a).length();
}

class Object {
public:
    __host__ __device__ virtual bool ray_intersect (const Ray& ray, HitRecord &rec) const = 0;
    bool emitted;
};

class Sphere : public Object {
public:
    Vec3f center;
    float radius;
    Material *material;
    
    __host__ __device__ Sphere() {}
    __host__ __device__ Sphere(const Vec3f &c, const float r, Material *m, bool emitted=false) : center(c), radius(r), material(m) {
        this->emitted = emitted;
    }

    __host__ __device__ bool ray_intersect(const Ray& ray, HitRecord &rec) const override {
        Vec3f L = center - ray.origin();
        float tca = dot(L,ray.direction());
        float d2 = dot(L,L) - tca*tca;
        if (d2 > radius*radius) return false;
        float thc = sqrtf(radius*radius - d2);
        rec.t       = tca - thc;
        float t1 = tca + thc;
        if (rec.t < 0) rec.t = t1;
        if (rec.t < 0) return false;

        rec.p = ray.point(rec.t);
        rec.n = (rec.p - center) / radius;
        rec.m = material;
        rec.emitted = emitted;
        return true;
    }
};

class Triangle : public Object
{
public:
    __host__ __device__ Triangle() {}
    __host__ __device__ Triangle(const Vec3f &a, const Vec3f &b, const Vec3f &c, Material *face_mat) : a(a), b(b), c(c) {
        ab_is_edge = false;
        ac_is_edge = false;
        bc_is_edge = false;
        this->face_mat = face_mat;
        this->edge_mat = nullptr;
        edge_width = 0.0;
        this->emitted = false;
    }

    __host__ __device__ Triangle(const Vec3f &a, const Vec3f &b, const Vec3f &c, Material *face_mat, Material *edge_mat) : a(a), b(b), c(c) {
        ab_is_edge = true;
        ac_is_edge = true;
        bc_is_edge = true;
        this->face_mat = face_mat;
        this->edge_mat = edge_mat;
        edge_width = 0.05;
    }

    __host__ __device__ bool ray_intersect(const Ray& ray, HitRecord &rec) const override {
        Vec3f e1 = b - a;
        Vec3f e2 = c - a;
        Vec3f pvec = cross(ray.direction(), e2);
        float det = dot(e1, pvec);

        if ( -1e-6 < det && det < 1e-6 )
        {
            return false;
        }

        Vec3f tvec = ray.origin() - a;
        float u = dot(tvec, pvec) / det;
        if ( u < 0 || u > 1 )
        {
            return false;
        }

        Vec3f qvec = cross(tvec, e1);
        float v = dot(ray.direction(), qvec) / det;
        if ( v < 0 || u + v > 1 )
        {
            return false;
        }

        rec.t = dot(e2, qvec) / det;
        if ( rec.t < T_MIN || rec.t > T_MAX )
        {
            return false;
        }

        rec.n = cross(e1, e2);
        rec.n.make_unit_vector();
        bool flip_normal = dot(ray.direction(), rec.n) > 0;
        if ( flip_normal )
        {
            rec.n = (-1) * rec.n;
        }
        rec.p = ray.point(rec.t);
        rec.m = face_mat;
        float dist = 0.0;
        dist = cross(b - a, rec.p - a).length() / (b - a).length();
        if ( dist < edge_width and ab_is_edge )
        {
            rec.m = edge_mat;
        }
        dist = cross(c - a, rec.p - a).length() / (c - a).length();
        if ( dist < edge_width and ac_is_edge )
        {
            rec.m = edge_mat;
        }
        dist = cross(c - b, rec.p - b).length() / (c - b).length();
        if ( dist < edge_width and bc_is_edge )
        {
            rec.m = edge_mat;
        }
        rec.u = u;
        rec.v = v;
        return true;
    }

    Vec3f a;
    Vec3f b;
    Vec3f c;

    float edge_width;
    bool ab_is_edge;
    bool ac_is_edge;
    bool bc_is_edge;
    Material *face_mat;
    Material *edge_mat;
};


class Rectangle : public Object 
{
public:
    __host__ __device__ Rectangle(Vec3f *v, Material *m)
    {
        t = new Triangle[2];
        a = v[0];
        b = v[1];
        d = v[3];
        t[0] = Triangle(v[1], v[0], v[3], m);
        t[1] = Triangle(v[3], v[2], v[1], m);
        w = (d - a).length();
        h = (b - a).length();
    }

    __host__ __device__ bool ray_intersect(const Ray& ray, HitRecord &rec) const override 
    {
        rec.t = T_MAX;
        HitRecord tmp_rec;
        bool hit = false;
        if (t[0].ray_intersect(ray, tmp_rec) && tmp_rec.t < rec.t) {
            rec = tmp_rec;
            hit = true;
        }
        if (t[1].ray_intersect(ray, tmp_rec) && tmp_rec.t < rec.t) {
            rec = tmp_rec;
            hit = true;
        }
        if ( hit ) // calculate uv-coords of rec.p
        {
            float dist_to_ab = Dist(rec.p, a, b);
            float dist_to_ad = Dist(rec.p, a, d);
            rec.u = dist_to_ab / w;
            rec.v = dist_to_ad / h;
        }
        return hit;
    }
    Vec3f a, b, d;
    float w, h;       
    Triangle *t;
};


class Tetrahedron: public Object
{
public:
    __host__ __device__ Tetrahedron(const Vec3f &center, float radius, Material *face_mat, Material *edge_mat, int n) :
        center(center), radius(radius), face_mat(face_mat), edge_mat(edge_mat) {
        
        Vec3f v[4] = {{+1, 0, -1/sqrtf(2)},
                      {-1, 0, -1/sqrtf(2)},
                      {0, +1, +1/sqrtf(2)},
                      {0, -1, +1/sqrtf(2)}};
        int edges[][2] = {
            {0, 1},
            {0, 2},
            {0, 3},
            {1, 2},
            {1, 3},
            {2, 3}
        };
        for (Vec3f &vertex: v) {
            vertex = vertex.normalize();
            vertex = radius * vertex + center;
        }
        triangles = new Triangle[4];
        triangles[0] = Triangle(v[0], v[1], v[3], face_mat, edge_mat);
        triangles[1] = Triangle(v[1], v[2], v[3], face_mat, edge_mat);
        triangles[2] = Triangle(v[2], v[0], v[3], face_mat, edge_mat);
        triangles[3] = Triangle(v[0], v[1], v[2], face_mat, edge_mat);
        for (Vec3f &vertex: v) {
            vertex = vertex - center;
            vertex /= radius;
            vertex = vertex * (radius - 0.05) + center;
        }
        spheres_cnt = n * 6;
        spheres = new Sphere[n * 6];
        for (int i = 0; i < 6; ++i) {
            Vec3f edge(v[edges[i][1]] - v[edges[i][0]]);
            float edge_length = edge.length();
            float step = edge_length / (n + 1);
            edge.make_unit_vector();
            for(int j = 0; j < n; ++j)
            {
                Vec3f sphere_center = v[edges[i][0]] + (j + 1) * step * edge;
                spheres[i*n + j] = Sphere(sphere_center, 0.02, nullptr, true);
                
            }
        }
        this->emitted = false;
    }

    __host__ __device__ bool ray_intersect(const Ray& ray, HitRecord &rec) const override {
        rec.t = T_MAX;
        bool hit = false;
        HitRecord tmp_rec;
        for (int i = 0; i < 4; ++i) {
            if ( triangles[i].ray_intersect(ray, tmp_rec) && tmp_rec.t < rec.t) {
                rec = tmp_rec;
                hit = true;
            }
        }
        for(int i = 0; i < spheres_cnt; ++i) {
            if (spheres[i].ray_intersect(ray, tmp_rec) && tmp_rec.t < rec.t) {
                rec = tmp_rec;
                hit = true;
            }
        }
        return hit;
    }

    Vec3f center;
    float radius;
    Material *face_mat;
    Material *edge_mat;
    Triangle *triangles;
    int spheres_cnt;
    Sphere *spheres;
};

class Icosahedron : public Object {
public:
    __host__ __device__ Icosahedron(const Vec3f &center, float radius, Material *face_mat, Material *edge_mat, int n) : 
        center(center), radius(radius), face_mat(face_mat), edge_mat(edge_mat) {
            float phi = (1 + sqrtf(5)) / 2;
            Vec3f v[] = {
                {-1, +phi, 0},
                {+1, +phi, 0},
                {+1, -phi, 0},
                {-1, -phi, 0},
                {0, +1, -phi}, 
                {0, +1, +phi},
                {0, -1, -phi},
                {0, -1, +phi},
                {-phi, 0, +1},
                {+phi, 0, +1},
                {+phi, 0, -1},
                {-phi, 0, -1}
            };
            for (Vec3f &vertex: v) {
                vertex = vertex.normalize();
                vertex = radius * vertex + center;
            }


            triangles = new Triangle[20];
            
            int indicies[][3] = {
                {0, 1, 4},
                {0, 1, 5},
                {0, 4, 11},
                {0, 11, 8}, // 3
                {0, 5, 8}, // 4
                {5, 7, 8}, // 5
                {5, 7, 9}, 
                {5, 9, 1},
                {1, 9, 10},
                {1, 10, 4},
                {4, 6, 10},
                {4, 6, 11},
                {2, 7, 3},
                {2, 3, 6},
                {2, 9, 10},
                {2, 9, 7},
                {2, 6, 10},
                {3, 8, 11}, // 17
                {3, 7, 8}, // 18
                {3, 6, 11}
            };

            for (int i = 0; i < 20; ++i) {
                triangles[i] = Triangle(v[indicies[i][0]], v[indicies[i][1]], v[indicies[i][2]], face_mat, edge_mat);
            }

            int edges[30][2] = {
                {0, 1},
                {0, 4},
                {0, 5},
                {0, 11},
                {0, 8},
                {1, 4},
                {1, 5},
                {1, 9},
                {1, 10},
                {2, 3},
                {2, 6},
                {2, 7},
                {2, 9},
                {2, 10},
                {3, 6},
                {3, 7},
                {3, 8},
                {3, 11},
                {4, 6},
                {4, 10},
                {4, 11},
                {5, 7},
                {5, 8},
                {5, 9},
                {6, 10},
                {6, 11},
                {7, 8},
                {7, 9},
                {8, 11},
                {9, 10}
            };

            spheres_cnt = 30 * n;
            spheres = new Sphere[spheres_cnt];
            for (Vec3f &vertex: v) {
                vertex = vertex - center;
                vertex /= radius;
                vertex = vertex * (radius - 0.05) + center;
            }
            
            for (int i = 0; i < 30; ++i) {
                Vec3f edge(v[edges[i][1]] - v[edges[i][0]]);
                float edge_length = edge.length();
                float step = edge_length / (n + 1);
                edge.make_unit_vector();
                for(int j = 0; j < n; ++j)
                {
                    Vec3f sphere_center = v[edges[i][0]] + (j + 1) * step * edge;
                    spheres[i*n + j] = Sphere(sphere_center, 0.02, nullptr, true); 
                }
            }
    }

    __host__ __device__ bool ray_intersect(const Ray& ray, HitRecord &rec) const override {
        rec.t = T_MAX;
        bool hit = false;
        HitRecord tmp_rec;
        for(int i = 0; i < 20; ++i) {
            if (triangles[i].ray_intersect(ray, tmp_rec) && tmp_rec.t < rec.t) {
                hit = true;
                rec = tmp_rec;
            }
        }
        for(int i = 0; i < spheres_cnt; ++i) {
            if (spheres[i].ray_intersect(ray, tmp_rec) && tmp_rec.t < rec.t) {
                rec = tmp_rec;
                hit = true;
            }
        }
        return hit;
    }

    Vec3f center;
    float radius;
    Material *face_mat;
    Material *edge_mat;
    Triangle *triangles;
    int spheres_cnt;
    Sphere *spheres;
};

class Pentagon : public Object {
public:
    __host__ __device__ Pentagon() {}
    __host__ __device__ Pentagon(Vec3f &v0, Vec3f &v1, Vec3f &v2, Vec3f &v3, Vec3f &v4, Material *face_mat, Material *edge_mat) :
        face_mat(face_mat), edge_mat(edge_mat) {
            triangles = new Triangle[3];
            triangles[0] = Triangle(v0, v1, v4, face_mat, edge_mat);
            triangles[0].bc_is_edge = false;
            triangles[1] = Triangle(v1, v2, v4, face_mat, edge_mat);
            triangles[1].ac_is_edge = false;
            triangles[1].bc_is_edge = false;
            triangles[2] = Triangle(v2, v3, v4, face_mat, edge_mat);
            triangles[2].ac_is_edge = false;
    }

    __host__ __device__ bool ray_intersect(const Ray &ray, HitRecord &rec) const override {
        rec.t = T_MAX;
        bool hit = false;
        HitRecord tmp_rec;
        for (int i = 0; i < 3; ++i) {
            if (triangles[i].ray_intersect(ray, tmp_rec) && tmp_rec.t < rec.t) {
                hit = true;
                rec = tmp_rec;
            }
        }
        return hit;
    }
    
    Triangle *triangles;
    Material *face_mat;
    Material *edge_mat;
};

class Dodecahedron : public Object {
public:
    __host__ __device__ Dodecahedron(const Vec3f &center, float radius, Material *face_mat, Material *edge_mat, int n) :
        center(center), radius(radius), face_mat(face_mat), edge_mat(edge_mat) {
        float phi = (1 + sqrtf(5)) / 2;
        Vec3f v[] = {
            {-1, -1, +1},
            {+1, -1, +1},
            {-1, +1, +1},
            {+1, +1, +1},
            {-1, -1, -1},
            {+1, -1, -1},
            {-1, +1, -1},
            {+1, +1, -1},

            {0, -1/phi, phi},
            {0, +1/phi, phi},
            {0, -1/phi, -phi},
            {0, +1/phi, -phi},

            {-1/phi, -phi, 0},
            {+1/phi, -phi, 0},
            {-1/phi, +phi, 0},
            {+1/phi, +phi, 0},

            {-phi, 0, +1/phi},
            {-phi, 0, -1/phi},
            {+phi, 0, +1/phi},
            {+phi, 0, -1/phi}
        };

        for (Vec3f &vertex: v) {
            vertex = vertex.normalize();
            vertex = vertex * radius + center;
        }

        pentagons = new Pentagon[FACES];

        pentagons[0] = Pentagon(v[18], v[1], v[8], v[9], v[3], face_mat, edge_mat);
        pentagons[1] = Pentagon(v[16], v[0], v[8], v[9], v[2], face_mat, edge_mat);
        pentagons[2] = Pentagon(v[9], v[2], v[14], v[15], v[3], face_mat, edge_mat);
        pentagons[3] = Pentagon(v[8], v[0], v[12], v[13], v[1], face_mat, edge_mat);
        pentagons[4] = Pentagon(v[13], v[1], v[18], v[19], v[5], face_mat, edge_mat);
        pentagons[5] = Pentagon(v[15], v[3], v[18], v[19], v[7], face_mat, edge_mat);
        pentagons[6] = Pentagon(v[12], v[0], v[16], v[17], v[4], face_mat, edge_mat);
        pentagons[7] = Pentagon(v[14], v[2], v[16], v[17], v[6], face_mat, edge_mat);
        pentagons[8] = Pentagon(v[11], v[6], v[14], v[15], v[7], face_mat, edge_mat);
        pentagons[9] = Pentagon(v[10], v[4], v[12], v[13], v[5], face_mat, edge_mat);
        pentagons[10] = Pentagon(v[19], v[5], v[10], v[11], v[7], face_mat, edge_mat);
        pentagons[11] = Pentagon(v[17], v[4], v[10], v[11], v[6], face_mat, edge_mat);

        int edges[EDGES][2] = {
            {0, 8},
            {0, 12},
            {0, 16},
            {1, 8},
            {1, 13},
            {1, 18},
            {2, 9},
            {2, 14},
            {2, 16},
            {3, 9},
            {3, 15},
            {3, 18},
            {4, 10},
            {4, 12},
            {4, 17},
            {5, 10},
            {5, 13},
            {5, 19},
            {6, 11},
            {6, 14},
            {6, 17},
            {7, 11},
            {7, 15},
            {7, 19},
            {8, 9},
            {10, 11},
            {12, 13},
            {14, 15},
            {16, 17},
            {18, 19}
        };
        spheres_cnt = 30 * n;
        spheres = new Sphere[spheres_cnt];
        for (Vec3f &vertex: v) {
            vertex = vertex - center;
            vertex /= radius;
            vertex = vertex * (radius - 0.05) + center;
        }
            
        for (int i = 0; i < 30; ++i) {
            Vec3f edge(v[edges[i][1]] - v[edges[i][0]]);
            float edge_length = edge.length();
            float step = edge_length / (n + 1);
            edge.make_unit_vector();
            for(int j = 0; j < n; ++j)
            {
                Vec3f sphere_center = v[edges[i][0]] + (j + 1) * step * edge;
                spheres[i*n + j] = Sphere(sphere_center, 0.02, nullptr, true); 
            }
        }
    }

    __host__ __device__ bool ray_intersect(const Ray &ray, HitRecord &rec) const override {
        rec.t = T_MAX;
        bool hit = false;
        HitRecord tmp_rec;
        for (int i = 0; i < 12; ++i)
        {
            if (pentagons[i].ray_intersect(ray, tmp_rec) && tmp_rec.t < rec.t) {
                hit = true;
                rec = tmp_rec;
            }
        }
        for(int i = 0; i < spheres_cnt; ++i) {
            if (spheres[i].ray_intersect(ray, tmp_rec) && tmp_rec.t < rec.t) {
                rec = tmp_rec;
                hit = true;
            }
        }
        return hit;
    }

    const static int EDGES = 30;
    const static int VERTICES = 20;
    const static int FACES = 12;

    Vec3f center;
    float radius;
    Pentagon *pentagons;
    Material *face_mat;
    Material *edge_mat;
    int spheres_cnt;
    Sphere *spheres;
};

#endif //PGP_CW_V3_OBJECT_H
