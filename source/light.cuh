#ifndef PGP_CW_V3_LIGHT_H
#define PGP_CW_V3_LIGHT_H

#include "vector.cuh"

class Light {
public:
    __host__ __device__ Light(const Vec3f &p, const float i) : position(p), intensity(i) {}
    Vec3f position;
    float intensity;
};

#endif //PGP_CW_V3_LIGHT_H
