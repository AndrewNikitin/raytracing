#ifndef PGP_CW_V3_MATERIAL_H
#define PGP_CW_V3_MATERIAL_H

#include "vector.cuh"
#include "texture.cuh"

class Material {
public:
    __host__ __device__ Material(const float r, const Vec4f &a, Texture *texture, const float spec) : 
        refractive_index(r), albedo(a), texture(texture), specular_exponent(spec) {}
    __host__ __device__ Material() : refractive_index(1), albedo(1,0,0,0), texture(nullptr), specular_exponent() {}
    __host__ __device__ Vec3f Diffuse(float u, float v) {
        if (texture == nullptr)
            return Vec3f(0.0, 0.0, 0.0);
        return texture->value(u, v);
    }
    float refractive_index;
    float specular_exponent;
    Vec4f albedo;
    Texture *texture;
};


__host__ __device__ Vec3f reflect(const Vec3f &I, const Vec3f &N) {
    return I - N*2.f*dot(I,N);
}

__host__ __device__ Vec3f refract(const Vec3f &I, const Vec3f &N, const float eta_t, const float eta_i=1.f) { // Snell's law
    float cosi = - max(-1.f, min(1.f, dot(I,N)));
    if (cosi<0) return refract(I, -N, eta_i, eta_t); // if the ray comes from the inside the object, swap the air and the media
    float eta = eta_i / eta_t;
    float k = 1 - eta*eta*(1 - cosi*cosi);
    return k<0 ? Vec3f(1,0,0) : I*eta + N*(eta*cosi - sqrtf(k)); // k<0 = total reflection, no ray to refract. I refract it anyways, this has no physical meaning
}

#endif //PGP_CW_V3_MATERIAL_H
