#ifndef PGP_CW_V3_RAY_H
#define PGP_CW_V3_RAY_H

#include "vector.cuh"

class Ray {
public:
    __host__ __device__ Ray() {}
    __host__ __device__ Ray(const Vec3f &a, const Vec3f &b) : A(a), B(b) {}

    __host__ __device__ Vec3f origin() const { return A; }
    __host__ __device__ Vec3f direction() const { return B; }
    __host__ __device__ Vec3f point(float t) const { return A + t*B; }

    Vec3f A;
    Vec3f B;
};

#endif //PGP_CW_V3_RAY_H
