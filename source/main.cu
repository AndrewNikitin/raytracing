#define _USE_MATH_DEFINES
#include <cmath>
#include <limits>
#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>


#include "vector.cuh"
#include "light.cuh"
#include "material.cuh"
#include "object.cuh"
#include "image.cuh"
#include "camera.cuh"
#include "ssaa.cuh"

#include "gpu_renderer.cuh"
#include "cpu_renderer.cuh"

void RenderUsingCpu(Scene &scene, std::string &path_to_texture, int frames, Camera &camera, int depth, int rays_per_pixel)
{
    Image floor_texture = ReadPPM(path_to_texture);
    scene.textures = (Texture**)malloc(4 * sizeof(Texture*));
    scene.objects = (Object**)malloc(scene.objects_num * sizeof(Object*));
    scene.lights = (Light**)malloc(scene.n_lights * sizeof(Light*));
    scene.materials = (Material**)malloc(scene.materials_num * sizeof(Material*));
    CPURenderer::CreateWorld(scene, floor_texture);

    uchar4 *buffer;
    buffer = (uchar4 *)malloc(scene.width * scene.height * sizeof(uchar4));

    float delta_t = 2 * M_PI / frames;

    for (int frame = 0; frame < frames; ++frame)
    {
        CPURenderer::Render(buffer, scene, camera, delta_t * frame); 
        SaveAsPPM("test_output/" + std::to_string(frame) + ".ppm", buffer, scene.width, scene.height);
        std::cerr << "Render " << frame + 1 << " out of " << frames << std::endl;
    }
    std::cerr << "Complete" << std::endl;
}

void RenderUsingGpu(Scene &scene, std::string &path_to_texture, int frames, Camera &camera, int depth, int rays_per_pixel)
{
    cudaDeviceSetLimit(cudaLimitMallocHeapSize,20000000*sizeof(double));
    cudaDeviceSetLimit(cudaLimitStackSize,12928);
    Image floor_texture = ReadPPM(path_to_texture);
    ImageToGPU(floor_texture);
    CSC(cudaMalloc(&scene.textures, 4 * sizeof(Texture*)));
    CSC(cudaMalloc(&scene.objects, scene.objects_num * sizeof(Object*)));
    CSC(cudaMalloc(&scene.lights, scene.n_lights * sizeof(Light*)));
    CSC(cudaMalloc(&scene.materials, scene.materials_num * sizeof(Material*)));
    GPURenderer::CreateWorld<<<1,1>>>(scene, floor_texture);
    CSC(cudaGetLastError());

    uchar4 *buffer;
    uchar4 *host_buffer = (uchar4*)malloc(scene.width * scene.height * sizeof(uchar4));
    CSC(cudaMalloc(&buffer, scene.width * scene.height * sizeof(uchar4)));

    float delta_t = 2 * M_PI / frames;

    for (int frame = 0; frame < frames; ++frame)
    {
        GPURenderer::Render<<<1,1>>>(buffer, scene, camera, delta_t * frame);
        CSC(cudaGetLastError());    
        CSC(cudaMemcpy(host_buffer, buffer, scene.width * scene.height * sizeof(uchar4), cudaMemcpyDeviceToHost));
        SaveAsPPM("test_output/" + std::to_string(frame) + ".ppm", host_buffer, scene.width, scene.height);
        std::cerr << "Render " << frame + 1 << " out of " << frames << std::endl;
    }
    std::cerr << "Complete" << std::endl;
}

int main(int argc, char **argv) {
    bool use_cpu = false;
    if (argc == 2 && std::string(argv[1]) == "--cpu")
    {
        use_cpu = true;
        std::cerr << "Render using CPU";
    }
    
    int frames;
    float fov;
    std::string path_to_output;
    float r0c, z0c, f0c, Arc, Azc, wrc, wzc, wfc, prc, pzc;
    float r0n, z0n, f0n, Arn, Azn, wrn, wzn, wfn, prn, pzn;
    std::string path_to_texture;
    Vec3f floor_color;
    float floor_ref;
    int depth, rays_per_pixel;

    Scene scene;
    scene.objects_num = 4;
    scene.materials_num = 4;
    std::cin >> frames;
    std::cin >> path_to_output;
    std::cin >> scene.width >> scene.height >> fov;
    
    std::cin >> r0c >> z0c >> f0c >> Arc >> Azc >> wrc >> wzc >> wfc >> prc >> pzc;
    std::cin >> r0n >> z0n >> f0n >> Arn >> Azn >> wrn >> wzn >> wfn >> prn >> pzn;
    
    std::cin >> scene.center[0][0] >> scene.center[0][2] >> scene.center[0][1] \
             >> scene.color[0][0] >> scene.color[0][1] >> scene.center[0][2] \
             >> scene.radius[0] >> scene.refract[0] >> scene.transparency[0] >> scene.lights_per_edge[0];
    std::cin >> scene.center[1][0] >> scene.center[1][2] >> scene.center[1][1] \
             >> scene.color[0][0] >> scene.color[0][1] >> scene.center[0][2] \
             >> scene.radius[1] >> scene.refract[1] >> scene.transparency[1] >> scene.lights_per_edge[1];
    std::cin >> scene.center[2][0] >> scene.center[2][2] >> scene.center[2][1] \
             >> scene.color[0][0] >> scene.color[0][1] >> scene.center[0][2] \
             >> scene.radius[2] >> scene.refract[2] >> scene.transparency[2] >> scene.lights_per_edge[2];
    for (int i = 0; i < 4; ++i)
    {
        std::cin >> scene.floor_coords[i][0] >> scene.floor_coords[i][2] >> scene.floor_coords[i][1];
    }
    std::cin >> path_to_texture >> floor_color[0] >> floor_color[1] >> floor_color[2] >> floor_ref;
    std::cin >> scene.n_lights;
    for (int i = 0; i < scene.n_lights; ++i)
    {
        std::cin >> scene.light_pos[i][2] >> scene.light_pos[i][0] >> scene.light_pos[i][1];
        std::cin >> scene.light_col[i][0] >> scene.light_col[i][1] >> scene.light_col[i][2];
    }
    std::cin >> depth >> rays_per_pixel;

    Camera camera(
            r0c, z0c, f0c, Arc, Azc, wrc, wzc, wfc, prc, pzc,
            r0n, z0n, f0n, Arn, Azn, wrn, wzn, wfn, prn, pzn,
            fov, scene.width, scene.height
            );

    if (use_cpu)
    {
        RenderUsingCpu(scene, path_to_texture, frames, camera, depth, rays_per_pixel);
    }
    else 
    {
        RenderUsingGpu(scene, path_to_texture, frames, camera, depth, rays_per_pixel);
    }
    return 0;
}

