#ifndef PGP_CW_V3_CAMERA_H
#define PGP_CW_V3_CAMERA_H

#include <cmath>

#include "vector.cuh"
#include "ray.cuh"

__host__ __device__
float degrees_to_radians(float degrees)
{
    return degrees * M_PI / 180;
}

class Camera
{
private:
    float r0c, z0c, f0c, Arc, Azc, wrc, wzc, wfc, prc, pzc;
    float r0n, z0n, f0n, Arn, Azn, wrn, wzn, wfn, prn, pzn;
    float fov;
    int width, height;

    __host__ __device__
    Vec3f GetPosition(float t)
    {
        float r = r0c + Arc * sinf(wrc * t + prc);
        float z = z0c + Azc * sinf(wzc * t + pzc);
        float f = f0c + wfc * t;

        Vec3f pos(
            r * cos(f), // r * sinf(f),
            z,
            r * sin(f)  // z
        );

        return pos;
    }

    __host__ __device__
    Vec3f GetDirection(float t)
    {
        float r = r0n + Arn * sinf(wrn * t + prn);
        float z = z0n + Azn * sinf(wzn * t + pzn);
        float f = f0n + wfn * t;

        Vec3f dir(
            r * cos(f), // r * sinf(f),
            z,
            r * sin(f)  // z
        );

        return dir;
    }

public:
    __host__ __device__
    Camera(float r0c, float z0c, float f0c, float Arc, float Azc, float wrc, float wzc, float wfc, float prc, float pzc,
           float r0n, float z0n, float f0n, float Arn, float Azn, float wrn, float wzn, float wfn, float prn, float pzn,
           float fov, int width, int height) 
    {
        this->r0c = r0c;
        this->z0c = z0c;
        this->f0c = f0c;
        this->Arc = Arc;
        this->Azc = Azc;
        this->wrc = wrc;
        this->wzc = wzc;
        this->wfc = wfc;
        this->prc = prc;
        this->pzc = pzc;
        
        this->r0n = r0n;
        this->z0n = z0n;
        this->f0n = f0n;
        this->Arn = Arn;
        this->Azn = Azn;
        this->wrn = wrn;
        this->wzn = wzn;
        this->wfn = wfn;
        this->prn = prn;
        this->pzn = pzn;

        this->fov = fov;
        this->width = width;
        this->height = height;
    }

    __host__ __device__
    Ray GetRay(double x, double y, double t)
    {
        Vec3f lookat = GetDirection(t);
        Vec3f lookfrom = GetPosition(t);
        Vec3f vup(0, -1, 0);
        float aspect_ratio = float(width) / height;

        float theta = degrees_to_radians(fov);
        float half_height = tan(theta/2);
        float half_width = aspect_ratio * half_height;

        Vec3f w, u, v;
        w = (lookfrom - lookat).normalize();
        u = cross(vup, w).normalize();
        v = cross(w, u);

        Vec3f lower_left_corner = lookfrom - half_width*u - half_height*v - w;
        Vec3f horizontal = 2*half_width*u;
        Vec3f vertical = 2*half_height*v;

        return Ray(lookfrom, (lower_left_corner + x*horizontal + y*vertical - lookfrom).normalize());
    }
};


#endif // PGP_CW_V3_CAMERA_H
