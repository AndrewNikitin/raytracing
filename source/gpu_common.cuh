#ifndef PGP_CW_V3_GPU_COMMON_H
#define PGP_CW_V3_GPU_COMMON_H

#define CSC(call)  					                                \
do {								                                \
	cudaError_t res = call;			                                \
	if (res != cudaSuccess) {		                                \
		fprintf(stderr, "ERROR in %s:%d. Message: %s\n",			\
				__FILE__, __LINE__, cudaGetErrorString(res));		\
		exit(0);					                                \
	}								                                \
} while(0);

#endif // PGP_CW_V3_GPU_COMMON_H