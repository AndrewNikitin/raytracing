#ifndef PGP_CW_TEXTURE_H
#define PGP_CW_TEXTURE_H

#include "image.cuh"
#include "vector.cuh"
#include "gpu_common.cuh"

class Texture
{
public:
    __host__ __device__ virtual Vec3f value(float u, float v) const = 0;
};

class ConstantTexture : public Texture
{
public:
    __host__ __device__ ConstantTexture(const Vec3f &color) : color(color) {}
    __host__ __device__ Vec3f value(float u, float v) const override {
        return color;
    }
    Vec3f color;
};

class GradientTexture : public Texture
{
public:
    __host__ __device__ GradientTexture() {}
    __host__ __device__ Vec3f value(float u, float v) const override {
        return Vec3f(u, v, 0.0f);
    }
};

class ImageTexture : public Texture
{
public:
    __host__ __device__ ImageTexture() : buffer(nullptr), w(0), h(0), on_gpu(false) {}
    __host__ __device__ ImageTexture(int w, int h, uchar4 *buffer, bool on_gpu=false, bool copy=false) {
        this->w = w;
        this->h = h;
        this->on_gpu = on_gpu;
        if (copy) {
            this->buffer = (uchar4 *)malloc(sizeof(uchar4) * w * h);
            for (int i = 0; i < w * h; ++i) {
                this->buffer[i] = buffer[i];
            }
        } else {
            this->buffer = buffer;
        }
    }

    __host__ __device__ Vec3f value(float u, float v) const override {
        if ( buffer == nullptr )
        {
            return Vec3f(0.0, 1.0, 1.0);
        }

        int i = (int)(u * h);
        int j = (int)(v * w);

        i = max(min(i, h), 0);
        j = max(min(j, w), 0);

        int pixel_start_idx = w * i + j;
        unsigned char r = buffer[pixel_start_idx].x;
        unsigned char g = buffer[pixel_start_idx].y;
        unsigned char b = buffer[pixel_start_idx].z;
        float scale = 1.0f / 255.0f;

        return Vec3f(scale * r, scale * g, scale * b);
    }

    uchar4 *buffer;
    int w;
    int h;
    bool on_gpu;
};

#endif