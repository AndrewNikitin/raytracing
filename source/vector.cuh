#ifndef PGP_CW_V3_VECTOR_H
#define PGP_CW_V3_VECTOR_H

#include <iostream>
#include <cmath>


class Vec3f {
public:
    __host__ __device__ Vec3f() {}
    __host__ __device__ Vec3f(float e0, float e1, float e2) { e[0] = e0; e[1] = e1; e[2] = e2; }
    __host__ __device__ inline float x() const { return e[0]; }
    __host__ __device__ inline float y() const { return e[1]; }
    __host__ __device__ inline float z() const { return e[2]; }
    __host__ __device__ inline float r() const { return e[0]; }
    __host__ __device__ inline float g() const { return e[1]; }
    __host__ __device__ inline float b() const { return e[2]; }

    __host__ __device__ inline const Vec3f& operator+() const { return *this; }
    __host__ __device__ inline Vec3f operator-() const { return Vec3f(-e[0], -e[1], -e[2]); }
    __host__ __device__ inline float operator[](int i) const { return e[i]; }
    __host__ __device__ inline float& operator[](int i) { return e[i]; }

    __host__ __device__ inline Vec3f& operator+=(const Vec3f &v2);
    __host__ __device__ inline Vec3f& operator-=(const Vec3f &v2);
    __host__ __device__ inline Vec3f& operator*=(const Vec3f &v2);
    __host__ __device__ inline Vec3f& operator/=(const Vec3f &v2);
    __host__ __device__ inline Vec3f& operator*=(const float t);
    __host__ __device__ inline Vec3f& operator/=(const float t);

    __host__ __device__ inline float length() const { return sqrt(e[0]*e[0] + e[1]*e[1] + e[2]*e[2]); }
    __host__ __device__ inline float squared_length() const { return e[0]*e[0] + e[1]*e[1] + e[2]*e[2]; }
    __host__ __device__ inline void make_unit_vector();
    __host__ __device__ inline Vec3f normalize();

    float e[3];
};


inline std::istream& operator>>(std::istream &is, Vec3f &t) {
    is >> t.e[0] >> t.e[1] >> t.e[2];
    return is;
}

inline std::ostream& operator<<(std::ostream &os, const Vec3f &t) {
    os << t.e[0] << " " << t.e[1] << " " << t.e[2];
    return os;
}

__host__ __device__ inline void Vec3f::make_unit_vector() {
    float k = 1.0 / sqrt(e[0]*e[0] + e[1]*e[1] + e[2]*e[2]);
    e[0] *= k; e[1] *= k; e[2] *= k;
}

__host__ __device__ inline Vec3f Vec3f::normalize() {
    float k = 1.0f / sqrt(e[0]*e[0] + e[1]*e[1] + e[2]*e[2]);
    return {e[0] * k, e[1] * k, e[2] *k};
}

__host__ __device__ inline Vec3f operator+(const Vec3f &v1, const Vec3f &v2) {
    return Vec3f(v1.e[0] + v2.e[0], v1.e[1] + v2.e[1], v1.e[2] + v2.e[2]);
}

__host__ __device__ inline Vec3f operator-(const Vec3f &v1, const Vec3f &v2) {
    return Vec3f(v1.e[0] - v2.e[0], v1.e[1] - v2.e[1], v1.e[2] - v2.e[2]);
}

__host__ __device__ inline Vec3f operator*(const Vec3f &v1, const Vec3f &v2) {
    return Vec3f(v1.e[0] * v2.e[0], v1.e[1] * v2.e[1], v1.e[2] * v2.e[2]);
}

__host__ __device__ inline Vec3f operator/(const Vec3f &v1, const Vec3f &v2) {
    return Vec3f(v1.e[0] / v2.e[0], v1.e[1] / v2.e[1], v1.e[2] / v2.e[2]);
}

__host__ __device__ inline Vec3f operator*(float t, const Vec3f &v) {
    return Vec3f(t * v.e[0], t * v.e[1], t * v.e[2]);
}

__host__ __device__ inline Vec3f operator/(Vec3f v, float t) {
    return Vec3f(v.e[0] / t, v.e[1] / t, v.e[2] / t);
}

__host__ __device__ inline Vec3f operator*(const Vec3f &v, float t) {
    return Vec3f(t * v.e[0], t * v.e[1], t * v.e[2]);
}

__host__ __device__ inline float dot(const Vec3f &v1, const Vec3f &v2) {
    return v1.e[0] * v2.e[0]
           + v1.e[1] * v2.e[1]
           + v1.e[2] * v2.e[2];
}

__host__ __device__ inline Vec3f cross(const Vec3f &v1, const Vec3f &v2) {
    return Vec3f(v1.e[1] * v2.e[2] - v1.e[2] * v2.e[1],
                v1.e[2] * v2.e[0] - v1.e[0] * v2.e[2],
                v1.e[0] * v2.e[1] - v1.e[1] * v2.e[0]);
}

__host__ __device__ inline Vec3f& Vec3f::operator+=(const Vec3f &v){
    e[0] += v.e[0];
    e[1] += v.e[1];
    e[2] += v.e[2];
    return *this;
}

__host__ __device__ inline Vec3f& Vec3f::operator*=(const Vec3f &v){
    e[0] *= v.e[0];
    e[1] *= v.e[1];
    e[2] *= v.e[2];
    return *this;
}

__host__ __device__ inline Vec3f& Vec3f::operator/=(const Vec3f &v){
    e[0] /= v.e[0];
    e[1] /= v.e[1];
    e[2] /= v.e[2];
    return *this;
}

__host__ __device__ inline Vec3f& Vec3f::operator-=(const Vec3f& v) {
    e[0] -= v.e[0];
    e[1] -= v.e[1];
    e[2] -= v.e[2];
    return *this;
}

__host__ __device__ inline Vec3f& Vec3f::operator*=(const float t) {
    e[0] *= t;
    e[1] *= t;
    e[2] *= t;
    return *this;
}

__host__ __device__ inline Vec3f& Vec3f::operator/=(const float t) {
    float k = 1.0f/t;

    e[0] *= k;
    e[1] *= k;
    e[2] *= k;
    return *this;
}

__host__ __device__ inline Vec3f unit_vector(Vec3f v) {
    return v / v.length();
}

class Vec4f {
public:
    __host__ __device__ Vec4f() {}
    __host__ __device__ Vec4f(float e0, float e1, float e2, float e3) { e[0] = e0; e[1] = e1; e[2] = e2; e[3] = e3; }
    __host__ __device__ inline float x() const { return e[0]; }
    __host__ __device__ inline float y() const { return e[1]; }
    __host__ __device__  inline float z() const { return e[2]; }
    __host__ __device__ inline float w() const { return e[3]; }

    __host__ __device__ inline float operator[](int i) const { return e[i]; }
    __host__ __device__ inline float& operator[](int i) { return e[i]; }

    float e[4];
};

#endif //PGP_CW_V3_VECTOR_H
