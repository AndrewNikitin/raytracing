#ifndef PGP_CW_V3_UTILS_H
#define PGP_CW_V3_UTILS_H

#include <iostream>
#include <fstream>
#include <vector>

#include "vector.cuh"
#include "gpu_common.cuh"

const int IMAGE_PPM_FORMAT = 1;
const int IMAGE_DATA_FORMAT = 2;

struct Image {
    int w;
    int h;
    uchar4 *buffer;
};

__host__ void ImageToGPU(Image &image)
{
    uchar4 *d_buffer;
    CSC(cudaMalloc(&d_buffer, sizeof(uchar4) * image.w * image.h));
    CSC(cudaMemcpy(d_buffer, image.buffer, sizeof(uchar4) * image.w * image.h, cudaMemcpyHostToDevice));
    free(image.buffer);
    image.buffer = d_buffer;
}

__host__ void SaveAsPPM(const std::string &path_to_file, const uchar4 *frame_buffer, int w, int h)
{
    std::ofstream ofs(path_to_file);
    ofs << "P6\n" << w << " " << h << "\n255\n";
    for (int i = 0; i < h * w; ++i) {
            ofs << (char) frame_buffer[i].x;
            ofs << (char) frame_buffer[i].y;
            ofs << (char) frame_buffer[i].z;
    }
    ofs.close();
}

__host__ void SaveAsData(const std::string &path_to_file, const uchar4 *frame_buffer, int w, int h)
{
    std::ofstream ofs(path_to_file, std::ios::binary);
    ofs.write((char*)&w, sizeof(w));
    ofs.write((char*)&h, sizeof(h));
    ofs.write((char*)frame_buffer, sizeof(uchar4) * w * h);
}

__host__ Image ReadPPM(const std::string &path_to_file)
{
    Image image;
    std::ifstream ifs(path_to_file);
    std::string code;
    int max_value;
    ifs >> code >> image.w >> image.h >> max_value;
    if (max_value != 255 || code != "P6") {
        std::cerr << "WARNING! Unsupported image format may cause undefined behavior!\n";
    }
    image.buffer = (uchar4 *)malloc(sizeof(uchar4) * image.w * image.h);
    ifs.get(); // to avoid reading '\n' after max_value
    for (int i = 0; i < image.w * image.h; ++i)
    {
        image.buffer[i].x = ifs.get();
        image.buffer[i].y = ifs.get();
        image.buffer[i].z = ifs.get();
        image.buffer[i].w = 0;
    }
    return image;
}

__host__ Image ReadData(const std::string &path_to_file)
{
    Image image;
    std::ifstream ifs(path_to_file);
    ifs.read((char*)&image.w, sizeof(int));
    ifs.read((char*)&image.h, sizeof(int));
    image.buffer = (uchar4 *)malloc(sizeof(uchar4) * 4);
    ifs.read((char*)image.buffer, image.w * image.h * sizeof(uchar4));
    return image;
}


#endif //PGP_CW_V3_UTILS_H
