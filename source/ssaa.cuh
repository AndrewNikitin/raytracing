#ifndef PGP_CW_V3_SSAA_H
#define PGP_CW_V3_SSAA_H

__global__ void SSAA(uchar4 *src, uchar4 *dst, int w, int h, int sw, int sh)
{
    int idx = threadIdx.x + blockIdx.x * blockDim.x;
    int offset = blockDim.x * gridDim.x;
    int scaled_size = (w / sw) * (h / sh);
    for (int i = idx; i < scaled_size; i += offset)
    {
        int xscaled = i % (w / sw);
        int yscaled = i / (w / sw);
        int x = xscaled * sw;
        int y = yscaled * sh;
        for (int dy = 0; dy < sh; ++dy)
        {
            uint3 summ = make_uint3(0, 0, 0);
            for (int dx = 0; dx < sw; ++dx)
            {
                summ.x += src[(y + dy) * w  + (x + dx)].x;
                summ.y += src[(y + dy) * w  + (x + dx)].y;
                summ.z += src[(y + dy) * w  + (x + dx)].z;
            }
            dst[yscaled * (w / sw) + xscaled] = make_uchar4(summ.x/(sw*sh), summ.y/(sw*sh), summ.z/(sw*sh), 0);
        }
    }
}

#endif
