#ifndef PGP_CW_V3_CPU_RENDERER_H 
#define PGP_CW_V3_CPU_RENDERER_H

#include "object.cuh"
#include "scene.cuh"

#include <cstdlib>


namespace CPURenderer {
    __host__ bool SceneIntersect(Scene &scene, const Ray &ray, HitRecord &rec);
    __host__ Vec3f CastRay(Scene &scene, const Ray &ray, int depth);
    __host__ void Render(uchar4 *buffer, Scene scene, Camera camera, float t);
    __host__ void CreateWorld(Scene scene, Image floor_texture);
};

__host__ void CPURenderer::Render(uchar4 *buffer, Scene scene, Camera camera, float t)
{
    for (int k = 0; k < scene.width * scene.height; k += 1)
    {
        int i = k % scene.width;
        int j = k / scene.width;
        float x = double(i) / (scene.width-1);
        float y = double(j) / (scene.height - 1); 
        Ray ray = camera.GetRay(x, y, t);
        Vec3f color = CPURenderer::CastRay(scene, ray, 0);
        int r = 255 * fmax(0.0f, fmin(1.f, color[0]));
        int g = 255 * fmax(0.0f, fmin(1.f, color[1]));
        int b = 255 * fmax(0.0f, fmin(1.f, color[2]));
        buffer[i + j*scene.width] = make_uchar4(r, g, b, 0);
    }
}

__host__ Vec3f CPURenderer::CastRay(Scene &scene, const Ray &ray, int depth)
{
    HitRecord rec;
    if (depth > 4 || !CPURenderer::SceneIntersect(scene, ray, rec))
    {
        return {0.1, 0.1, 0.1};
    }
    
    if ( rec.emitted || rec.m == nullptr )
    {
        return {1.0, 1.0, 1.0};
    }
    
    Vec3f reflect_dir = reflect(ray.direction(), rec.n).normalize();
    Vec3f refract_dir = refract(ray.direction(), rec.n, rec.m->refractive_index).normalize();
    Vec3f reflect_orig = dot(reflect_dir,rec.n) < 0 ? rec.p - rec.n*1e-3 : rec.p + rec.n*1e-3;
    Vec3f refract_orig = dot(refract_dir,rec.n) < 0 ? rec.p - rec.n*1e-3 : rec.p + rec.n*1e-3;
    Vec3f reflect_color = CastRay(scene, Ray(reflect_orig, reflect_dir), depth + 1);
    Vec3f refract_color = CastRay(scene, Ray(refract_orig, refract_dir), depth + 1);

    float diffuse_light_intensity = 0, specular_light_intensity = 0;
    for (int i = 0; i < scene.n_lights; ++i)
    {
        Vec3f light_dir = (scene.lights[i]->position - rec.p).normalize();
        float light_distance = (scene.lights[i]->position - rec.p).length();

        Vec3f shadow_orig = dot(light_dir,rec.n) < 0 ? rec.p - rec.n*1e-3 : rec.p + rec.n*1e-3;
        HitRecord shadow_hit;
        Ray shadow_ray(shadow_orig, light_dir); 
        if (CPURenderer::SceneIntersect(scene, shadow_ray, shadow_hit) && (shadow_hit.p-shadow_orig).length() < light_distance)
        {
            if (shadow_hit.m != nullptr && shadow_hit.m->refractive_index > 1)
                diffuse_light_intensity  += 0.7 * scene.lights[i]->intensity * max(0.f, dot(light_dir,rec.n));
        }
        else
        {
            diffuse_light_intensity  += scene.lights[i]->intensity * max(0.f, dot(light_dir,rec.n));
            specular_light_intensity += powf(max(0.f, dot(reflect(light_dir, rec.n),ray.direction())), rec.m->specular_exponent)*scene.lights[i]->intensity;
        }
    }
    Vec3f diffuse_color = rec.m->Diffuse(rec.u, rec.v) * diffuse_light_intensity * rec.m->albedo[0];
    Vec3f specular_color = Vec3f(1., 1., 1.) * specular_light_intensity * rec.m->albedo[1];
    return  diffuse_color + specular_color + reflect_color*rec.m->albedo[2] + refract_color*rec.m->albedo[3];
}


__host__ bool CPURenderer::SceneIntersect(Scene &scene, const Ray &ray, HitRecord &rec)
{
    bool hit = false;
    rec.t = T_MAX;
    HitRecord tmp_rec;
    for (int i = 0; i < scene.objects_num; ++i)
    {
        if (scene.objects[i]->ray_intersect(ray, tmp_rec) && tmp_rec.t < rec.t )
        {
            hit = true;
            rec = tmp_rec;
        }
    }
    return hit;
}

__host__ void CPURenderer::CreateWorld(Scene scene, Image floor_texture)
{
    scene.textures[3] = new ImageTexture(floor_texture.w, floor_texture.h, floor_texture.buffer, true, true);
    //scene.textures[3] = new GradientTexture();
    scene.textures[2] = new ConstantTexture(Vec3f(0.3, 0.1, 0.1));
    scene.textures[1] = new ConstantTexture(Vec3f(0.6, 0.7, 0.8));
    scene.textures[0] = new ConstantTexture(Vec3f(0.4, 0.4, 0.3));

    scene.materials[0] = new Material(1.0, Vec4f(0.6, 0.3, 0.1, 0.0), scene.textures[0], 50.f); // glass
    scene.materials[1] = new Material(1.05, Vec4f(0.0, 0.5, 0.1, 0.8), scene.textures[1], 125.f); // ivory
    scene.materials[2] = new Material(1.0, Vec4f(0.9,  0.1, 0.0, 0.0), scene.textures[2], 10.); // red_rubber
    scene.materials[3] = new Material(1.0, Vec4f(0.9,  0.1, 0.0, 0.0), scene.textures[3], 10.);    

    scene.objects[0] = new Rectangle(scene.floor_coords, scene.materials[3]);
    scene.objects[1] = new Icosahedron(scene.center[0], scene.radius[0], scene.materials[1], scene.materials[2], scene.lights_per_edge[0]);
    scene.objects[2] = new Dodecahedron(scene.center[1], scene.radius[1], scene.materials[1], scene.materials[2], scene.lights_per_edge[1]); 
    scene.objects[3] = new Tetrahedron(scene.center[2], scene.radius[2], scene.materials[1], scene.materials[2], scene.lights_per_edge[2]);

    for (int i = 0; i < scene.n_lights; ++i)
    {
        scene.lights[i] = new Light(scene.light_pos[i], 1.5);
    }
}

#endif // PGP_CW_V3_CPU_RENDERER_H