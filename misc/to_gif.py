import os
import sys
import argparse

import imageio

SUPPORTED_FORMATS = set(
    ['ppm', 'data']
)

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--path", type=str, action='store')
    parser.add_argument("--save-to", type=str, action='store')
    parser.add_argument("--format", type=str, action='store')
    args = parser.parse_args()

    if args.format not in SUPPORTED_FORMATS:
        print("Unsupported format type")
        sys.exit(0)

    images = []
    for filename in os.listdir(args.path):
        if filename.endswith(f".{args.format}"):
            image_num = int(filename.split('.')[0])
            images.append((image_num, imageio.imread(args.path + '/' + filename)))
    with imageio.get_writer(args.save_to, mode='I', fps=20) as writer:
        for num, image in sorted(images):
            print(type(image))
            writer.append_data(image)

if __name__ == '__main__':
    main()
