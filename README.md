# RAYTRACING

## Introduction

Course work on discipline "Parallel Data Processing" - simple
raytracing implementation on CPU and GPU.

## Sources

1. [256 строчек голого C++: пишем трассировщик лучей с нуля за несколько часов](https://habr.com/ru/post/436790/)
2. [Ray Tracing in One Weekend](https://raytracing.github.io/)

## Output examples:

Current version:
![](output/output4.jpg "Output4")

## TODO:

1. Fix CUDA implementation
2. Code optimization
